from DAO.AppDAO import AppDAO
import requests, json
class AppController:
    def __init__(self):
        self.vaListaPlaneta = []
    def CarregarPlaneta(self):
         appDAO = AppDAO()
         appDAO.carregar()
    def ListarPlaneta(self):
        appDAO = AppDAO()
        vaLista = list()
        for vaPlaneta in appDAO.Listar():
            vaNumberFilms = self.NumeroFilmes(vaPlaneta["name"])
            print(vaNumberFilms)
            vaPlaneta["NumberFilms"] = vaNumberFilms[0]
            vaLista.append(vaPlaneta)
        return vaLista
    def ListarPlanetaNome(self, query):
        appDAO = AppDAO()
        vaLista = list()
        for vaPlaneta in appDAO.ListarNome(query):
            vaNumberFilms = self.NumeroFilmes(vaPlaneta["name"])
            print(vaNumberFilms)
            vaPlaneta["NumberFilms"] = vaNumberFilms[0]
            vaLista.append(vaPlaneta)
        return vaLista
    def ListarPlanetaId(self, query):
        appDAO = AppDAO()
        vaLista = list()
        for vaPlaneta in appDAO.ListarId(query):
            vaNumberFilms = self.NumeroFilmes(vaPlaneta["name"])
            print(vaNumberFilms)
            vaPlaneta["NumberFilms"] = vaNumberFilms[0]
            vaLista.append(vaPlaneta)
        return vaLista
    def RemoverPlaneta(self, query):
        appDAO = AppDAO()
        appDAO.Remover(query)
    def InserirPlaneta(self, query):
        appDAO = AppDAO()
        appDAO.Inserir(query)
    def NumeroFilmes(self, Nome):
        if len(self.vaListaPlaneta) == 0:
            page = "https://swapi.co/api/planets/?page=1"
            response = requests.get(page)
            y = json.loads(response.text)
            while y["next"] is not None:
                response = requests.get(page)
                y = json.loads(response.text)
                print(y["next"])
                 # print(type(y["results"]));
                page = y["next"]
                for a in y["results"]:
                    self.vaListaPlaneta.append({"name": a["name"], "Numberfilms": len(a["films"])})
        vaNumberFilms = [vaPlaneta["Numberfilms"] for vaPlaneta in self.vaListaPlaneta if vaPlaneta["name"] == Nome]
        return vaNumberFilms