from Util.Connection import Connection
import requests, json

class AppDAO:
    def carregar(self):
        conexao = Connection()
        conn = conexao.open()
        col = conexao.createDb(conn)
        vaListaPlaneta = []
        page = "https://swapi.co/api/planets/?page=1"
        response = requests.get(page)
        y = json.loads(response.text)
        while y["next"] is not None:
            response = requests.get(page)
            y = json.loads(response.text)
            print(y["next"])
            # print(type(y["results"]));
            page = y["next"]
            for a in y["results"]:
                vaListaPlaneta.append({"name": a["name"], "climate": a["climate"], "terrain": a["terrain"]})
            print(vaListaPlaneta)
        col.insert(vaListaPlaneta)

    def Listar(self):
        conexao = Connection()
        conn = conexao.open()
        col = conexao.createDb(conn)
        return list(col.find())

    def ListarNome(self, query):
        conexao = Connection()
        conn = conexao.open()
        col = conexao.createDb(conn)
        return list(col.find(query))

    def ListarId(self, query):
        conexao = Connection()
        conn = conexao.open()
        col = conexao.createDb(conn)
        return list(col.find(query))

    def Remover(self, query):
        conexao = Connection()
        conn = conexao.open()
        col = conexao.createDb(conn)
        col.delete_many(query)

    def Inserir(self, query):
        conexao = Connection()
        conn = conexao.open()
        col = conexao.createDb(conn)
        item_dict = json.dumps(query)
        if len(item_dict) > 1:
            col.insert_many(query)
        else:
            col.insert_one(query)