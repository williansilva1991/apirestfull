import pymongo
class Connection:
    def open(self):
        con = pymongo.MongoClient("mongodb://localhost:27017/")
        return con
    def createDb(self,conn):
        dbList = conn.list_database_names()
        global db
        global col
        if not "Starwars" in dbList:
           db = conn["starwars"]
        else:
            db = conn.get_database("starwars")
        collectionList = db.list_collection_names()
        if not "planetas" in collectionList:
            col = db["palnetas"]
        else:
            col = conn.get_database("starwars").get_collection("planetas")
        return col