from flask import Flask, request, jsonify
from Controller.AppController import AppController
import json
from bson import json_util
from bson.objectid import ObjectId

app = Flask(__name__)

devs = [
    {
        'nome': 'Willian Dantas',
        'lang': 'python'
    },
    {
        'nome': 'Mariana',
        'lang': '.Net'
    }
]

@app.route('/Planeta', methods=['GET'])
def home():
    controller = AppController()
    controller.CarregarPlaneta()
    return jsonify([{"mensagem":"Carga completa!"}]);

@app.route('/Planeta/Listar', methods=['GET'])
def listar():
    controller = AppController()
    lista = controller.ListarPlaneta()
    return json.dumps(lista, default=json_util.default)

@app.route('/Planeta/ListarNome', methods=['GET'])
def dev_listarnome():
    controller = AppController()
    dados = request.json
    query = {"name": {"$regex": "^"+dados["name"]+""}}
    lista = controller.ListarPlanetaNome(query)
    return json.dumps(lista, default=json_util.default)



@app.route('/Planeta/ListarId', methods=['GET'])
def dev_listarid():
    controller = AppController()
    dados = request.json
    id = dados["_id"]
    query = {"_id": ObjectId(id["$oid"])}
    lista = controller.ListarPlanetaId(query)
    return json.dumps(lista, default=json_util.default)



@app.route('/Planeta/Remover', methods=['POST'])
def dev_remover():
    controller = AppController()
    dados = request.json
    id = dados["_id"]
    query = {"_id": ObjectId(id["$oid"])}
    controller.RemoverPlaneta(query)
    return jsonify([{"mensagem":"Removido com sucesso!"}])

@app.route('/Planeta/Inserir', methods=['POST'])
def dev_inserte():
    controller = AppController()
    dados = request.json
    controller.InserirPlaneta(dados)
    return jsonify([{"mensagem": "Inserido com sucesso!"}])

if __name__ == 'main':
     app.run(debug=True)
